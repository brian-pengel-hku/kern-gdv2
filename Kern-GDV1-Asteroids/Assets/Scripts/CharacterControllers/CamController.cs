using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CamController : MonoBehaviour {
	
	[SerializeField]
	private Transform target ;
	[SerializeField]
	private float distance = 10 ;
	
	private void Update () {
		transform.LookAt(target) ;
		transform.position = target.position + -target.forward * distance ;
	}
}