﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : EntityController {
	
	[SerializeField]
	private float minVel = .3f,
		    	  maxVel = .8f;
	
	[SerializeField]
	private int size = 4 ;
	
	
	private void Awake () {
		base.Awake() ;
		velocity = Random.Range (minVel, maxVel) ;
		rotation = Random.Range (0f, 360f) * Mathf.Deg2Rad ;		
		
	}
	
	private void UpdateMesh () {
		if (size > 0) {
			EntityMesh = GameManager.meshes ["Enemy_size_" + size];
		}
	}
	
	// //////// //
	// Events
	// //////// //
	protected override void OnSpawn () {
		SetActive(true) ;
		UpdateMesh () ;
	}
	
	public override void OnCollision (EntityController other)  {
		if (CurrentState != EntityState.active) {
			return;
		}
		
		if (other.tag == "Enemy") {
			rotation *= -1;
		} else if (other.tag == "Projectile") {
			GameManager.Instance.Score += 20 ;
			End() ;
		}
	}
	
	protected override void OnOutOfBounds () {
		rotation *= -1 ;
	}
	
	// //////// //
	// Abstracts
	// //////// //
	protected override void Move () {
		x += velocity * Time.deltaTime * Mathf.Cos(rotation) ;
		y += velocity * Time.deltaTime * Mathf.Sin(rotation) * currentQuad.yEqualiser(y) ;
	}
	
	
	
	public void SetSize (int size) {
		this.size = size > 4 ? 4 : size ;
	}
	
	public void Immune () {
		CurrentState = EntityState.immune ;
		if (gameObject.active) {
			StartCoroutine (Blink (.5f));
		}
	}
	
	private IEnumerator Blink (float duration) {
		float end = Time.time + duration ;
		while (Time.time < end) {
			Renderer.enabled = !Renderer.enabled ;
			yield return new WaitForSeconds (.2f);
		}
		
		CurrentState = EntityState.active ;
		Renderer.enabled = true ;
	}
	
	private void End () {
		size -- ;
		
		if(size < 1) {
			GameManager.Instance.EnemyPool.Store(this) ;
			SetActive(false) ;
			return ;
		}
		
		int chuncks = 4 - size ;
		for (int cnt = 0; cnt < chuncks; cnt++) {
			CreateChunck (chuncks, cnt);
		}
		
		Immune () ;
		Spawn(x, y, rotation + 45f * Mathf.Deg2Rad) ;
	}
	
	private void CreateChunck (int max, int offset) {
		EnemyController chunck = GameManager.Instance.EnemyPool.Grab() ;
		chunck.SetSize(size) ;
		chunck.Spawn(x, y, rotation + 360f / max * offset * Mathf.Deg2Rad) ;
		chunck.Immune() ;
	}
	
}
