using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EntityState { active, inactive, immune }

public abstract class EntityController : ShapeWalker {
	
	[SerializeField]
	protected float radius = .06f,
					velocity,
					rotation;
	
	private MeshFilter filter;
	
	protected MeshRenderer Renderer { get; private set; }
	protected Transform Child 		{ get; private set; }
	public EntityState CurrentState	{ get; protected set; } 
	
	public float Radius { 
		get {
			return radius ;
		}
	}
	
	protected Mesh EntityMesh { 
		get {
			return filter.mesh ;
		}
		
		set {
			filter.mesh = value ;
		}
	}	
	
	// ///////////// //
	// Unity methods
	// ///////////// //
	protected void Awake () {
		CollisionManager.Instance.Entities.Add(this) ;

		Renderer = transform.GetComponentsInChildren<MeshRenderer>()[0] ;
		filter = transform.GetComponentsInChildren<MeshFilter>()[0] ;
		Child = transform.GetChild(0).transform ;
	}
	
	private void FixedUpdate () {
		if (CurrentState == EntityState.inactive) {
			return;
		}
		
		Move () ;
		
		if (attachedLevel.OutOfBounds (y)) {
			OnOutOfBounds ();
		}
		
		UpdatePosition () ;
		UpdateNormals () ;
		
		AlignToNormal(up) ;
		transform.position = Vector3.MoveTowards(attachedLevel.Translate(position), transform.position, Time.deltaTime) ;
	}
	
	// //////// //
	// Spawn
	// //////// //
	public void Spawn (float x, float y, float rotation) {
		this.rotation = rotation ;
		SetPosition(x, y) ;
		OnSpawn () ;
	}
	
	public void SetActive (bool b) {
		CurrentState = b ? EntityState.active : EntityState.inactive ;
		gameObject.SetActive(b) ;
	}
	
	
	// //////// //
	// Abstracts
	// //////// //
	protected abstract void Move () ;
	public abstract void OnCollision (EntityController other) ;
	
	// //////// //
	// Events
	// //////// //
	protected virtual void OnSpawn () {}
	protected virtual void OnOutOfBounds () {}
}