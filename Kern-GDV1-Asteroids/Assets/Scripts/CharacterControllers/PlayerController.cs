﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : EntityController {
	
	private const float maxRot = 360f * Mathf.Deg2Rad ;
	
	[SerializeField]
	private float accelleration = 5, 
				  maxVelocity = .05f,
				  rotationSpeed = 1.5f,
				  drag = 1.5f,
					
				  shootDelay = .5f,
				  lastShot ;
	
	[SerializeField]
	private int startLives = 3,
				lives ;
	
	// //////// //
	// Events
	// //////// //
	protected override void OnSpawn () {
		EntityMesh = GameManager.meshes["Player"] ;
		lives = startLives ;
		
		Camera.main.transform.LookAt(attachedLevel.transform.position);
		StartCoroutine (Blink(5f)) ;
	}
	
	public override void OnCollision (EntityController other) {
		if(CurrentState == EntityState.active && other.tag == "Enemy")
			Damaged () ;
	}
	
	// //////// //
	// Abstracts
	// //////// //
	protected override void Move () {
		if (CurrentState == EntityState.inactive) {
			return;
		}
		
		GetInput () ;
		ApplyVelocity () ;
		
		Child.localRotation = Quaternion.Euler(0, 0, -rotation * Mathf.Rad2Deg) ;
		SetCamera (position) ; // TMP
	}
	
	
	private void Damaged () {
		lives -- ;
		GameManager.Instance.Lives = lives ;
		
		if(lives < 1) {
			CurrentState = EntityState.inactive ;
			return ;
		}
			
		CurrentState = EntityState.immune ;
		StartCoroutine (Blink(3f)) ;
	}
	
	private void Shoot () {
		if (lastShot + shootDelay > Time.time || CurrentState == EntityState.inactive) {
			return;
		}
		
		ProjectileController p = GameManager.Instance.ProjectilePool.Grab() ;
		p.Spawn(x, y, rotation) ;
		p.SetActive(true) ;
		lastShot = Time.time ;
	}
	
	private IEnumerator Blink (float duration) {
		float end = Time.time + duration ;
		while (Time.time < end) {
			Renderer.enabled = !Renderer.enabled ;
			yield return new WaitForSeconds (.2f);
		}
		
		CurrentState = EntityState.active ;
		Renderer.enabled = true ;
	}

	
	
	private void GetInput () {
		velocity += (Input.GetAxis("Vertical") > 0 ? accelleration : -drag) * Time.deltaTime;
		velocity = Mathf.Clamp(velocity, 0, maxVelocity) ;
		
		rotation += rotationSpeed * -Input.GetAxis("Horizontal")  * Time.deltaTime ;
		rotation = rotation < 0 ? rotation + maxRot : rotation % maxRot ;
		
		if (Input.GetKey (KeyCode.Space)) {
			Shoot ();
		}
	}
	
	private void ApplyVelocity () {
		x += velocity * Mathf.Cos(rotation) ;
		y += velocity * Mathf.Sin(rotation) * currentQuad.yEqualiser(y) ;
	}
	
	private void SetCamera (Vector3 pos) {
		Camera.main.transform.position = attachedLevel.Translate(pos * 2.5f)  ;
		Camera.main.transform.LookAt(attachedLevel.Translate(pos));
	}
	
	
	
}
