using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ProjectileController : EntityController {
	
	[SerializeField]
	private float lifeTime ;
	
	private void Awake () {
		base.Awake () ;
		EntityMesh = GameManager.meshes["Player"] ;
	}
	
	private void End () {
		GameManager.Instance.ProjectilePool.Store(this) ;
		SetActive(false) ;
		CurrentState = EntityState.inactive ;
		return ;
	}
	
	// //////// //
	// Events
	// //////// //
	protected override void OnSpawn () {
		CurrentState = EntityState.active ;
	}
	
	public override void OnCollision (EntityController other)  {
		if (other.tag == "Enemy" && CurrentState == EntityState.active) {
			End ();
		}
	}
	
	protected override void OnOutOfBounds () {
		End() ;
	}

	// //////// //
	// Abstracts
	// //////// //
	protected override void Move () {
		x += velocity * Time.deltaTime * Mathf.Cos(rotation) ;
		y += velocity * Time.deltaTime * Mathf.Sin(rotation) * currentQuad.yEqualiser(y) ;
		Child.localRotation = Quaternion.Euler(0, 0, -rotation * Mathf.Rad2Deg) ;
	}
}