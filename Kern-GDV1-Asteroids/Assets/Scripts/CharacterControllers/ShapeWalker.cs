using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class ShapeWalker : MonoBehaviour {
	
	[SerializeField]
	protected Vector3 position;
	[SerializeField]
	protected Level attachedLevel ;
	protected Vector3 up, right ;
	protected Quad currentQuad ;
	protected float x, y;
	
	[SerializeField]
	protected float alignmentSpeed = 1.5f;
	
	public void AttachTo (Level level) {
		attachedLevel = level ;
		LimitPosition () ;
		currentQuad = new Quad(level.Shape, x, y) ;
	}
	
	// //////// //
	// Position
	// //////// //
	public void SetPosition (float x, float y) {
		this.x = x ;
		this.y = y ;
		
		UpdatePosition () ;
		UpdateNormals () ;
		transform.position = attachedLevel.Translate(position) ;
		transform.rotation = Quaternion.LookRotation(up);
	}
	
	protected void UpdatePosition () {
		LimitPosition () ;
		currentQuad.Update(x, y) ;		
		position = GetUpdatedPosition (x, y) ;
	}
	
	private Vector3 GetUpdatedPosition (float x, float y) {
		float xPosOnQuad = currentQuad.xOnQuad(x), 
			  yPosOnQuad = currentQuad.yOnQuad(y) ;
		
		Vector3 tmp1 = Vector3.Lerp(currentQuad.vec00, currentQuad.vec10, xPosOnQuad);
		Vector3 tmp2 = Vector3.Lerp(currentQuad.vec01, currentQuad.vec11, xPosOnQuad);
		return Vector3.Lerp(tmp1, tmp2, yPosOnQuad) ;
	}
	
	private void LimitPosition () {
		attachedLevel.Wrap (ref x) ;
		attachedLevel.Clamp (ref y) ;
	}
	
	
	// //////// //
	// Normals
	// //////// //
	protected void UpdateNormals () {
		up = currentQuad.GetQuadNormal(attachedLevel.LevelMesh.normals) ;
		up /= up.magnitude ;
		right = Vector3.Cross(up, Vector3.up).normalized ;
	}
	
	protected void AlignToNormal (Vector3 normal) {
		transform.rotation = Quaternion.Lerp(
			transform.rotation, 
			Quaternion.LookRotation(normal), 
			Time.deltaTime * alignmentSpeed
		) ;
	}
}