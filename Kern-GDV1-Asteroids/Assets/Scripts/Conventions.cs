//using UnityEngine;
//
////classes max 750 (try it!) -> Guideline = oversight + readability
//public class SomeClass : MonoBehaviour {
//    
//    //const
//    const int JUMP_HEIGHT = 10;//avoid magic numbers!
//    //static
//    static SomeClassManager ClassManager = new SomeClassManager();
//    
//    //variables
//    
//    #region spulVoorFeature1 //may for readability, but not because it must
//    //group by use
//    //there within public, protected, private
//    public int SomeProperty {
//        get {
//            //
//        }
//        set {
//            StartVeryExpensiveDebugFunction();
//            //
//        }
//    }
//    public int someBanaanInt;
//    //must because enormous hack + explanation
//    internal int someInternalInt;
//    
//    protected int someProtectedInt;
//    
//    int somePrivateInt;
//    private int somePrivateInt2; //<--
//    #endregion
//        
//    //functions, +always access modifier
//        
//    //firsy Unity Functions: Awake, Start, Update, LateUpdate, FixedUpdate
//        
//    //then the rest
//    public void SomePublicFunction(int arg1, int arg2) {
//        //code
//        bool condition;
//        //bool conditions: if ENUM is better, try that first
//        if (condition) {
//            return;
//        }
//        else {
//            
//        }
//        
//        bool a, b, c;
//        if (a && b && !c)      return;
//        else if (a && b && !c) return;
//        else if (a && b && !c) return;
//        else                   return; //result outlining
//            
//        //wildcard: switch statements
//    }
//}