using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionManager : UnitySingleton<CollisionManager> {
	
	public List<EntityController> Entities { get; private set ; }
	
	protected override void Awake () {
		base.Awake() ;
		Entities = new List<EntityController>() ;
	}

	// /////////////////////// //
	// MARKED for optimisation
	// /////////////////////// //
	public void CheckCollision () {
		int total = Entities.Count - 1 ;
		EntityController[] entities = Entities.ToArray() ;
		
		for (int cnt = 0; cnt < total; cnt ++) {
			EntityController current = entities[cnt] ;
			if (current.CurrentState != EntityState.active) {
				continue;
			}
			
			for (int i = cnt + 1; i < total ; i ++) {
				EntityController other = entities[i] ;
				if (other.CurrentState != EntityState.active) {
					continue;
				}
				
				if(Vector3.Distance(current.transform.position, other.transform.position) <= Mathf.Abs(current.Radius + other.Radius)) {
					current.OnCollision(other) ;
					other.OnCollision(current) ;
				}
			}
		}
	}
}