using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CollisionManager), typeof(UIManager))]
public class GameManager : UnitySingleton<GameManager> {

	[SerializeField] private GameObject playerPrefab ;
	[SerializeField] private GameObject projectile ;
	[SerializeField] private Level level ;
	
	private SpawnBehaviour[] spawnStrat ;
	private PlayerController player ;
	private int score, 
				lives ;
	
	
	public ObjectPool<ProjectileController> ProjectilePool { get; private set ; }
	public ObjectPool<EnemyController> EnemyPool { get; private set ; }
	
	public int Score { 
		get { 
			return score ; 
		}
		set { 
			score = value ; 
			UIManager.Instance.Display(UIEnum.score, score) ; 
		} 
	}
	
	public int Lives { 
		set { 
			lives = value ;
			UIManager.Instance.Display(UIEnum.lives, lives) ;
			
			if(lives == 0) {
				UIManager.Instance.GameOver(true) ;
			}
		} 
	}
	
	private void Start () {	
		spawnStrat = GetComponents<SpawnBehaviour>() ;
		GameManager.meshes = new Dictionary<string, Mesh> () ;
		SetMeshes() ;
		
		if(spawnStrat.Length > 0)
			EnemyPool = spawnStrat[0].Spawn(level) ;
		
		CreatePlayer () ;
		CreateProjectiles () ;
		
	}
	
	private void CreatePlayer () {
		player = Instantiate(playerPrefab, transform.position, Quaternion.identity).GetComponent<PlayerController> () ;
		player.AttachTo(level) ;
		player.Spawn(1, level.Height / 2, 90f * Mathf.Deg2Rad) ;
	}
	
	private void CreateProjectiles () {
		Transform parent = (new GameObject("Projectiles")).transform ; 
		ProjectilePool = new ObjectPool<ProjectileController> (() =>{
			ProjectileController projectile = Instantiate(
				this.projectile, 
				transform.position, 
				Quaternion.identity
			).GetComponent<ProjectileController> () ;
			
			projectile.transform.SetParent(parent) ;
			projectile.SetActive(false) ;
			
			projectile.AttachTo(level) ;
			return projectile ;
		}) ;
		
		ProjectilePool.Fill(50) ;
	}
	
	private void FixedUpdate () {
		if(Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
		if(Input.GetKeyDown(KeyCode.F1)) Application.LoadLevel(Application.loadedLevel);
		
		CollisionManager.Instance.CheckCollision () ;
	}
	
	// Future implementations would use scriptableobjects for the generated meshes
	// Currently harcoded to improve performance when instantiating enemies
	public static Dictionary<string, Mesh> meshes { get; private set; }
	
	private void SetMeshes () {
		meshes.Add("Enemy_size_4", (Mesh)SuperShape.Nut(16, 2, .5f)) ;
		meshes.Add("Enemy_size_3", (Mesh)SuperShape.Nut(12, 2, .5f)) ;
		meshes.Add("Enemy_size_2", (Mesh)SuperShape.Nut(8, 2, .5f)) ;
		meshes.Add("Enemy_size_1", (Mesh)SuperShape.Nut(4, 2, .5f)) ;
		meshes.Add("Player", (Mesh)SuperShape.Sphere(3, 2, .5f)) ;
	}
}