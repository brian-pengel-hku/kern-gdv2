﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class Level : MonoBehaviour {
	
	[SerializeField]
	private int levelWidth = 5, 
				levelHeight = 5;
	public float boundaryOffset;
	
	public PolarShape Shape { get; private set; }
	public Mesh LevelMesh { get; private set; }
	
	public int Height { get { return levelHeight; } }
	public int Width { get { return levelWidth; } }
	

	private void Awake () {
		Shape = SuperShape.Cylinder(levelWidth, levelHeight, .5f) ;		
		LevelMesh = GetComponent<MeshFilter>().mesh = (Mesh)Shape ;
		LevelMesh.RecalculateNormals() ;
	}
	
	public Vector3 Translate (Vector3 pos) {
		return transform.position + pos * transform.lossyScale.x ;
	}
	
	public void Wrap(ref float x) {
		if (x >= Shape.w) {
			x -= Shape.w;
		} 
		else if (x < 0) {
			x += Shape.w;
		}
	}
	
	public void Clamp(ref float y) {
		y = Mathf.Clamp(y, boundaryOffset, Shape.h - boundaryOffset) ;
	}
	
	public bool OutOfBounds(float y) {
		return y < boundaryOffset || y > Shape.h - boundaryOffset ;
	}
}






[System.Serializable]
public class Quad {
	[SerializeField] private int minX, minY, maxX, maxY ;
	
	public Vector3 vec00 { get { return points[v00] ; } }
	public Vector3 vec01 { get { return points[v01] ; } }
	public Vector3 vec10 { get { return points[v10] ; } }
	public Vector3 vec11 { get { return points[v11] ; } }
	
	public int v00 { get; private set; }
	public int v01 { get; private set; }
	public int v10 { get; private set; }
	public int v11 { get; private set; }
	
	private PolarShape shape ;
	private Vector3[] points ;
	
	// Distance between the 2 vertices at the top of the quad
	private float TopDist ;
	// Distance between the 2 vertices at the bottom of the quad
	private float BottomDist ;
	
	public float VerticalDist { get; private set; }
	

	public Quad (PolarShape shape, float x, float y) {
		this.shape = shape ;
		points = shape.GetPoints() ;
		Update(x, y) ;
	}
	
	public void Update (float x, float y) {
		int maxX = this.maxX = Mathf.CeilToInt(x),
			maxY = this.maxY = Mathf.CeilToInt(y),
			minX = this.minX = maxX - 1,
			minY = this.minY = maxY - 1 ;
		
		v00 = minX + minY * shape.HorVertices ;
		v10 = maxX + minY * shape.HorVertices ;
		v01 = minX + maxY * shape.HorVertices ;
		v11 = maxX + maxY * shape.HorVertices ;
		
		TopDist = Vector3.Distance(vec01, vec11) ;
		BottomDist = Vector3.Distance(vec00, vec10) ;
		VerticalDist = Vector3.Distance(vec00, vec01) ;
	}
	
	public float HorizontalDist (float y) {
		return Mathf.Lerp(BottomDist, TopDist, yOnQuad(y)) ; 
	}
	
	public float yEqualiser (float y) {
		return 1f / VerticalDist * HorizontalDist(y) ; 
	}
	
	public float xOnQuad (float x) {
		return Mathf.Abs(x - minX);
	}
	
	public float yOnQuad (float y) {
		return Mathf.Abs(y - minY);
	}
	
	public Vector3 GetSurfaceNormal (int a, int b, int c, Vector3[] normals) {
		Vector3 p1 = points[a], p2 = points[b], p3 = points[c],
				u = p2 - p1,
				v = p3 - p1;
				
		return new Vector3 (
			u.y * v.z - u.z * v.y,
			u.z * v.x - u.x * v.z,
			u.x * v.y - u.y * v.x
		) ;
	}
	
	// Quick implementation
	public Vector3 GetQuadNormal (Vector3[] normals) {
		Vector3 normal = Vector3.Lerp(GetSurfaceNormal(v00, v01, v11, normals), GetSurfaceNormal(v00, v11, v10, normals), .5f) ;
		return normal ; 
	}

	new public string ToString() {
		return "[" + v00 + ", " + v10 + ", " + v01 + ", " + v11 + "]" ;
	}
}