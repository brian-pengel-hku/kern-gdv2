using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum UIEnum { score, lives }

public class UIManager : UnitySingleton<UIManager> {
	[SerializeField] private Text scoreBoard ;
	[SerializeField] private Text livesBoard ;
	[SerializeField] private GameObject gameOver ;
	
	public void Display(UIEnum e, int val) {
		switch(e) {
			case UIEnum.score:
				if(scoreBoard != null) {
					scoreBoard.text = "Score: " + val ;
				}
				break;
			case UIEnum.lives:
				if(livesBoard != null) {
					livesBoard.text = "Lives: " + val ;
				}
				break;
		}
	}
	
	public void GameOver (bool b) {
		if(gameOver != null) {
			gameOver.SetActive(b) ;
		}
	}
}