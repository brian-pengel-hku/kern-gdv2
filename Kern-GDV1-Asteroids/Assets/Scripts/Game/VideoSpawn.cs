using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoSpawn : MonoBehaviour, SpawnBehaviour {
	[SerializeField]
	private GameObject asteroid ;
	
	[SerializeField]
	private int amount,
				preFill = 50 ;
	
	public ObjectPool<EnemyController> Spawn(Level level) {
		Transform parent = new GameObject("Enemies").transform ;
		
		ObjectPool<EnemyController> enemies = new ObjectPool<EnemyController> (()=>{ 
			return CreateAsteroid (asteroid, parent, level) ; 
		});
		
		enemies.Fill(preFill) ;
		
		for (int cnt = 0; cnt < amount; cnt++) {
			RandomSpawn (enemies.Grab (), level);
		}
		return enemies ;
	}
	
	private EnemyController CreateAsteroid(GameObject prefab, Transform parent, Level level) {
		EnemyController asteroid = Instantiate(prefab, Vector3.zero, Quaternion.identity).GetComponent<EnemyController> () ;
		asteroid.transform.SetParent(parent) ;	
		asteroid.SetActive(false) ;
		
		asteroid.AttachTo(level) ;
		return asteroid ;
	}
	
	private void RandomSpawn(EnemyController asteroid, Level level) {
		float randomX = Random.Range(2f, level.Width - 2f),
			  randomY = Random.Range(level.boundaryOffset, level.Height - level.boundaryOffset * 2),
			  randomRot = Random.Range(0f, 360f) * Mathf.Deg2Rad ;
		
		asteroid.Spawn(randomX, randomY, randomRot) ;
	}

}


public interface SpawnBehaviour {
	ObjectPool<EnemyController> Spawn(Level level) ;
	
	
}