using System;

public interface IPoolable {
	void onPool (UnityPool pool) ;
	void onStore () ;
	void onGrab () ;
}
