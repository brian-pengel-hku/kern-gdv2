using System;
using System.Collections.Generic;

// Nog niet optimaal, implementatie verliest link naar object wanner deze uit de pool wordt gehaald
public class ObjectPool <T> {
	
	private Stack<T> pooledObjects ;
	protected Func<T> factory ;
	
	public int Size {
		get { 
			return pooledObjects.Count ; 
		}
	}
	
	public ObjectPool () {
		pooledObjects = new Stack<T> () ;
		this.factory = ()=>{ return default(T) ; } ;
	}

	public ObjectPool (Func<T> factory) {
		pooledObjects = new Stack<T> () ;
		this.factory = factory ;
	}
	
	virtual public T Grab () {
		return (pooledObjects.Count == 0) ? 
			factory() :
			pooledObjects.Pop() ;
	}
	
	virtual public void Store (T obj) {
		pooledObjects.Push(obj);
	}
	
	public void Fill (int amount) {
		for (int cnt = 0; cnt < amount; cnt++) {
			Store (factory ());
		}
	}
	
	public void SetFactory (Func<T> factory) {
		this.factory = factory ;
	}
}