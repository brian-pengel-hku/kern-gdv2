using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityPool : ObjectPool <GameObject>{
	
	// ////////////////////////// //
	// STATIC UNITYPOOL
	// ////////////////////////// //
	protected static Dictionary<GameObject, UnityPool> instances ;

	public static UnityPool Instance (GameObject baseObject) {
		if (instances == null) {
			instances = new Dictionary<GameObject, UnityPool> ();
		}
		return !instances.ContainsKey(baseObject) ? 
					instances[baseObject] = new UnityPool (baseObject) : 
					instances[baseObject] ;
	}
	
	// ////////////////////////// //
	// UNITYPOOL
	// ////////////////////////// //
	private GameObject baseObject ;
	private Transform parent ;
	private bool containsPoolable ;
	
	private UnityPool (GameObject baseObject) {
		parent = (new GameObject("Object Pool (" + baseObject.name + ")")).transform;
		this.baseObject = baseObject ;
		this.factory = CreateObject ;
		
		containsPoolable = baseObject.GetComponents(typeof(IPoolable)).Length > 0 ;
	}
	
	private GameObject CreateObject () {
		GameObject obj = GameObject.Instantiate(baseObject, Vector3.zero, Quaternion.identity) ;
		obj.transform.SetParent(parent) ;

		if(containsPoolable) 
			obj.SendMessage("onPool", this, SendMessageOptions.DontRequireReceiver) ;

		obj.SetActive (false);
		return obj ;
	}
	
	override public void Store (GameObject obj) {
		if(containsPoolable) 
			obj.SendMessage("onStore", SendMessageOptions.DontRequireReceiver) ;
		
		obj.SetActive (false);
		base.Store(obj);
	}
	
	override public GameObject Grab () {
		GameObject obj = base.Grab();
		obj.SetActive (true);

		if(containsPoolable) 
			obj.SendMessage("onGrab", SendMessageOptions.DontRequireReceiver) ;
		return obj ;
	}
}
