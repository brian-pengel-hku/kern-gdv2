using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MeshBuilder {
	
	// Verts should be added clockwise
	public static int CreateTri (ref int[] tris, int index, int vA, int vB, int vC) {
		tris[index	  ] = (vA) ;
		tris[index + 1] = (vB) ;
		tris[index + 2] = (vC) ;		
		return index + 3 ;
	}
	
	// Vertices are coded by x and y (from 0,0 to 1,1)
	public static int CreateQuad (ref int[] tris, int index, int v00, int v10, int v01, int v11) {
		index = CreateTri(ref tris, index, v00, v01, v11);
		return  CreateTri(ref tris, index, v00, v11, v10);
	}
	
	//
	protected List<int> vertices ;
	public MeshBuilder () {
		Clear() ;
	}
	
	public int[] ToArray () { 
		return vertices.ToArray() ;
	}
	
	public void Clear () { 
		vertices = new List<int> () ; 
	}
	
	public void Add (int vertex) { 
		vertices.Add(vertex) ; 
	}
	
	public abstract int[] Build() ;
}


public class TriangleStrip : MeshBuilder {
	
	public override int[] Build () {
		int vertCount = vertices.Count,
			triCount = vertCount - 2,
			quadCount = (vertCount - 2) / 2;

		int[] tris = new int[(int)triCount * 3] ;

		int t = 0 ;
		for (int cnt = 0, v = 0; cnt < quadCount; cnt++, v += 2) {
			t = CreateQuad (ref tris, t, vertices [v], vertices [v + 1], vertices [v + 2], vertices [v + 3]);
		}

		if (vertCount % 2 != 0) {
			CreateTri (ref tris, t, vertices [vertCount - 3], vertices [vertCount - 1], vertices [vertCount - 2]);
		}

		return tris ;
	}
}



public class TriangleGrid : MeshBuilder {
	
	public int height { get; set; }
	public int width { get; set; }
	
	public TriangleGrid(int width, int height) {
		this.height = height ;
		this.width = width ;
	}
	
	public override int[] Build () {
		int vertCount = vertices.Count ;
		int[] tris = new int[(int)vertCount * 3] ;
		
		int t = 0 ;
		for (int y = 0, v = 0; y < height; y++, v += 2) {
			for (int x = 0; x < width; x++, v += 2) {
				t = CreateQuad (ref tris, t, vertices [v], vertices [v + 1], vertices [v + 2], vertices [v + 3]);
			}
		}
		

		return tris ;
	}
}