using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane : PolarShape {
	
	public Plane (int w, int h) : base (w, h) {}
	
	public override Vector3 GetPoint (float lat, float lon, float elevation) {
		return new Vector3(lat / w, elevation, lon / h) ;
	}
	
	public override Vector3 GetPoint (float lat, float lon) {
		return new Vector3(lat / w, 0, lon / h) ;
	}
}