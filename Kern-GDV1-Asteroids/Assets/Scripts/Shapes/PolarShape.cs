﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Shape defined by width (w) and height (h)
public abstract class PolarShape : Shape {	
	
	//
	public int VertVertices { get { return h + 1 ; } }
	public int HorVertices  { get { return w + 1 ; } }
	
	public int w { get; set; }
	public int h { get; set; }
	public float r { get; set; }
	
	public PolarShape (int w = 0, int h = 0, float r = .5f) {
		this.w = w ;
		this.h = h ;
		this.r = r ;
	}
	
	public abstract Vector3 GetPoint (float lon, float lat, float elevation) ;
	
	public override Vector3[] GetPoints () {
		int total = HorVertices * VertVertices ;
		Vector3[] points = new Vector3[total] ;
		
		for (int cnt = 0; cnt < total; cnt ++) {
			int i = cnt % HorVertices, 
				j = (cnt - i) / HorVertices ;
				points[cnt] = GetPoint(i, j) ;
		}
		
		return points ;
	}
	
	public override int[] BuildTris () {
		TriangleGrid grid = new TriangleGrid (w, h) ;
		for(int v = 0; v < HorVertices * VertVertices; v ++) {
			grid.Add(v + HorVertices) ;
			grid.Add(v) ;
		}
		
		return grid.Build() ;
	}
	
	// /////////// //
	// HELPERS
	// /////////// //
	
	public static void Cartesian2Polar(float x, float y, float z, out float r, out float lat, out float lon) {
		r = Mathf.Sqrt(x * x + y * y + z * z) ;
		lon = Mathf.Abs(Mathf.Acos(x / Mathf.Sqrt(x * x + y * y)));
		lat = Mathf.Acos(z / r) ;
	} 
	
}
