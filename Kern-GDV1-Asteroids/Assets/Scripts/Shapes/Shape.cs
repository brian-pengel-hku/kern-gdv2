﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Shape {

	// ----------------
	// Vars
	// ----------------
	public const float PI = Mathf.PI,
					   TWO_PI = PI * 2,
					   HALF_PI = PI / 2 ;

	// ----------------
	// Methods
	// ----------------
	protected Shape(/*, string name*/) {
//		this.name = name ;
	}
	
	// ----------------
	// Geometry gen..
	// ----------------
	// Get point on shape by longitude and latitude
	public abstract Vector3 GetPoint (float lon, float lat) ;
	
	// Get all points on shape
	public abstract Vector3[] GetPoints () ;
	
	
	// ----------------
	// Mesh generation
	// ----------------
	public static implicit operator Mesh(Shape shape) {
		Mesh mesh = new Mesh() ;
//		mesh.name = shape.name ;
		mesh.vertices = shape.GetPoints();
		mesh.triangles = shape.BuildTris() ;
		mesh.RecalculateNormals();
		return mesh;
	}
	
	// GenerateTris between vectors
	public abstract int[] BuildTris () ;
	
	// ----------------
	// Helpers
	// ----------------
	public static float Map(float val, float minA, float maxA, float minB, float maxB) {
		return (val - minA) / (maxA - minA) * (minB - maxB) + maxB;
	}

	public static int Map(int val, int minA, int maxA, int minB, int maxB) {
		return (val - minA) / (maxA - minA) * (minB - maxB) + maxB;
	}
	
	public static void PrintArray (int[] a) {
		string str = "[" ;
		foreach (int i in a) {
			str += i + ",";
		}
		Debug.Log(str + "]");
	}
}
