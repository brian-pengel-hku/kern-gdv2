using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere : PolarShape {

	
	public Sphere (int w, int h, int r = 1) : base (w, h) {	}
	
	public override Vector3 GetPoint(float lat, float lon, float elevation) {
		Vector3 point = GetPoint(lat, lon) ;
		point[0] *= elevation ;
		point[1] *= elevation ;
		point[2] *= elevation ;
		return point ;
	}
	
	public override Vector3 GetPoint (float lat, float lon) {
		float r = .5f ;
		lat = Shape.Map (lat, 0, w, -PI, PI);
		lon = Shape.Map (lon, 0, h, -HALF_PI, HALF_PI);
		
		float x = r * Mathf.Cos(lat) * Mathf.Cos(lon),
			  z = r * Mathf.Sin(lat) * Mathf.Cos(lon),
			  y = r * Mathf.Sin(lon) ;
		return new Vector3(x, y, z) ;
	}
}