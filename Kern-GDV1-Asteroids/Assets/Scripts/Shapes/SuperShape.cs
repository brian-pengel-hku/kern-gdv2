﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperShape : Sphere {
	
	public float n1 { get; set; }
	public float n2 { get; set; }
	public float n3 { get; set; }
	public float n4 { get; set; }
	public float n5 { get; set; }
	public float n6 { get; set; }
	public float m1 { get; set; }
	public float m2 { get; set; }
	public float a  { get; set; }
	public float b  { get; set; }
	
	public SuperShape (int w, int h, float r, float n1, float n2, float n3, float n4, float n5, float n6, float m1, float m2, float a = 1, float b = 1) : base (w, h) {
		this.n1 = n1 ;
		this.n2 = n2 ;
		this.n3 = n3 ;
		this.n4 = n4 ;
		this.n5 = n5 ;
		this.n6 = n6 ;
		this.m1 = m1 ;
		this.m2 = m2 ;
		this.a = a ;
		this.b = b ;
	}
	
	private float Modify (float theta, float n1, float n2, float n3, float m)  {
		float part1 = 1/a * Mathf.Cos(m * theta / 4) ;
		part1 = Mathf.Pow(Mathf.Abs(part1), n2);
		
		float part2 = 1/b * Mathf.Sin(m * theta / 4) ;
		part2 = Mathf.Pow(Mathf.Abs(part2), n3);
		
		return Mathf.Pow(part1 + part2, -1 / n1) ;
	}

	public override Vector3 GetPoint (float lat, float lon) {
		Vector3 og = base.GetPoint(lat, lon) ;
		
		lat = Shape.Map (lat, 0, w, -PI, PI);
		lon = Shape.Map (lon, 0, h, -HALF_PI, HALF_PI);
		
		float r1 = Modify(lat, n1, n2, n3, m1), 
			  r2 = Modify(lon, n4, n5, n6, m2);
		
		og[0] *= r1 * r2 ;
		og[2] *= r1 * r2 ;
		og[1] *= r2 ;

		return og ;
	}
	
	public static SuperShape Cube (int w, int h, float r) {
		return new SuperShape(w, h, r, 100, 100, 100, 100, 100, 100, 4, 4, 1, 1) ;
	}
	
	public static SuperShape Sphere (int w, int h, float r) {
		return new SuperShape(w, h, r, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1) ;
	}
	
	public static SuperShape Cylinder (int w, int h, float r) {
		return new SuperShape(w, h, r, 37.41f, -.24f, 19.07f, 100, 100, 100, 1, 4, 1.10f, 1.88f) ;
	}
	
	public static SuperShape Nut (int w, int h, float r) {
		return new SuperShape(w, h, r, 60f, 100f, 30f, 10, 10, 10, 8, 2) ;
	}
	
	public static SuperShape RandomShape (int w, int h, float r) {
		return new SuperShape(w, h, r, 
		  Random.Range(0, 100),
		  Random.Range(0, 100),
		  Random.Range(0, 100),
		  Random.Range(0, 100),
		  Random.Range(0, 100),
		  Random.Range(0, 100),
		  Random.Range(0, 8),
		  Random.Range(0, 8)
	 ) ;
	}
}
