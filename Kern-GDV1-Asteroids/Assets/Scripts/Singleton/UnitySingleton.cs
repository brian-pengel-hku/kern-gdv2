using UnityEngine ;

public abstract class UnitySingleton<T> : MonoBehaviour where T : Component {
	
	private static T instance ;
	
	public static T Instance {
		get {
			if (instance != null) {
				return instance;
			}
			
			if ((instance = FindObjectOfType<T> ()) == null) {
				GameObject obj = new GameObject ();
				obj.name = typeof(T).Name;
				instance = obj.AddComponent<T>();
			}
			
			return instance ;
		}
	}
	
	protected virtual void Awake () {
		if (instance == null) {
			instance = this as T ;
		}
	}
	
}